import { config } from "dotenv";
config();

export const database = {
  connectionLimit: 10,
  host: process.env.DATABASE_HOST || "localhost",
  user: process.env.DATABASE_USER || "taibt",
  password: process.env.DATABASE_PASSWORD || "laodai123",
  database: process.env.DATABASE_NAME || "dblinks"
};

export const port = process.env.PORT || 4000;
